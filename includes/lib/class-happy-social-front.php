<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class happy_social_Front {

  /**
   * The main plugin object.
   * @var   object
   * @access  public
   * @since   1.0.0
   */
  public $parent = null;

  /**
   * The REST API Response Status
   * @var   string json
   * @access  private
   * @since   1.0.0
   */
  private $status;

  /**
   * The json of selected settings
   * @var   string json
   * @access  public
   * @since   1.0.0
   */
  public $settings;

  /**
   * The main buttons block to output
   * @var   string
   * @access  public
   * @since   1.0.0
   */
  public $output;


  public function __construct ($parent) {
    $this->parent = $parent;
    add_action('plugins_loaded', array($this, 'init'));

  }

  /**
   * Get settings by GET (REST API)
   *
   * @since  1.0
   * @return void
   */
  public function init (){
    $url = '/hpscl/v1/hpscl_options';
    $request = new WP_REST_Request( 'GET', $url );
    $response = rest_do_request( $request );

    $this->settings = $response->get_data();
    $this->status = $response->get_status();// 200

    if($this->status === 200){
      $this->set_locations();
    }else{
      return new WP_Error( 'broke', __( "The settings are not REST available.", "happy-social" ) );
    }

    // Add SVG (hidden) in the footer
    add_action( 'wp_footer', array($this, 'get_icons') );

    // Create icons output
    $this->generate_output();
  }

  /**
   * Set up location
   *
   * Set up filters based on selected locations
   *
   * @since 1.0
   * @return void
   */
  public function set_locations(){

    $locations = $this->settings['hpscl_locations'];

    if( ! empty($locations)) {
      foreach ($locations as $location) {

        switch ($location) {
          case 'bt':
            add_filter( 'the_content', array($this, 'case_before_content'));
            break;
          case 'ac':
            add_filter( 'the_content', array($this, 'case_after_content'));
            break;
          case 'it':
            add_action( 'end_fetch_post_thumbnail_html', array($this, 'case_inside_thumbnails'));
            break;
          case 'lf':
            add_action( 'wp_footer', array($this, 'case_left_floating'));
            break;
          default:
            break;
        }
      }
    }
  }


  /**
   * Return social icons
   *
   * @since  1.0
   * @return string
   */
  public function get_icons() {
    $assets_dir = $this->parent->assets_dir;
    $style = $this->buttons_styles();
    $icons = file_get_contents( $assets_dir . '/icons/icons.svg' );
    echo $style . '<div style="display:none;visibility:hidden;">'. $icons .'</div>';
  }


  /**
   * Return social icons
   *
   * @since  1.0
   * @return string
   */
  public function generate_output() {
    $sizes = $this->buttons_sizes();
    $networks_order = trim( $this->settings['hpscl_networks_order'] );
    $networks = array_filter( explode(',', $networks_order ) );
    $output = '';
    if( ! empty( $networks_order ) ){
      $pre_svg = '<svg viewBox="0 0 48 48" '. $sizes .'><use xlink:href="#';
      $post_svg = '"></use></svg>';

      foreach ($networks as $network) {
        // Exclude whatsapp in mobile
        if( ! wp_is_mobile() && $network == 'whatsapp' ) continue;

        $icon = $pre_svg . $network . $post_svg;
        $output .= '<div class="'. $network .'_icon hpscl_icon">' . $icon .'</div>';
      }
    }

    $this->output = $output;
  }



  /**
   * Set up buttons before content
   *
   * @since  1.0
   * @return string
   */
  public function case_before_content ( $content ){
    // Add buttons after the content
    if ( is_singular() ){
      $type = get_post_type( get_the_ID() );
      $types = $this->settings['hpscl_post_types'];
      $output = $this->output;
      if ( in_array( $type, $types) ) {
        $content = '<div class="hpscl hpscl_before_content">'. $output .'</div>'. $content;
      }
    }

    // Returns the content.
    return $content;
  }

  /**
   * Set up buttons before content
   *
   * @since  1.0
   * @return string
   */
  public function case_after_content ( $content ){
    // Add buttons to the end of the content
    if ( is_singular() ){
      $type = get_post_type( get_the_ID() );
      $types = $this->settings['hpscl_post_types'];
      $output = $this->output;
      if ( in_array( $type, $types) ) {
        $content = $content . '<div class="hpscl hpscl_after_content">'. $output .'</div>';
      }
    }

    // Returns the content.
    return $content;
  }

  /**
   * Set up buttons inside the thumbnails
   *
   * @since  1.0
   * @return string
   */
  public function case_inside_thumbnails () {
    // Add buttons to the thumbnails
    $type = get_post_type( get_the_ID() );
    $types = $this->settings['hpscl_post_types'];
    if ( in_array( $type, $types) ) {
      $output = $this->output;
      $content = '<div class="hpscl hpscl_thumbnails">'. $output .'</div>';
      echo $content;
    }
  }

  /**
   * Set up buttons floating left side
   *
   * @since  1.0
   * @return string
   */
  public function case_left_floating (){
    // Add buttons to the footer
    if ( is_singular() ){
      $type = get_post_type( get_the_ID() );
      $types = $this->settings['hpscl_post_types'];
      $output = $this->output;

      $sizes = $this->buttons_sizes();
      $share_icon = '<svg viewBox="0 0 48 48" '. $sizes .'><use xlink:href="#share"></use></svg>';
      if ( in_array( $type, $types) ) {
        echo '<div class="hpscl hpscl_floating_left">'. $output . $share_icon .'</div>';
      }
    }
  }

  /**
   * Set buttons' color and size
   * @return string Inline CSS styles
   */
  private function buttons_sizes(){
    $size = $this->settings['hpscl_btn_size'];
    $sizes = '';
    $custom_size = '24';
    if( isset( $size) ){
      switch ($size) {
        case 'small':
          $custom_size = isset( $this->settings['hpscl_small_icon_size'] ) ? $this->settings['hpscl_small_icon_size'] : $custom_size;
          break;
        case 'medium':
          $custom_size = isset( $this->settings['hpscl_medium_icon_size'] ) ? $this->settings['hpscl_medium_icon_size'] : $custom_size;
          break;
        case 'large':
          $custom_size = isset( $this->settings['hpscl_large_icon_size'] ) ? $this->settings['hpscl_large_icon_size'] : $custom_size;
          break;
        default:
          break;
      }
      $sizes = 'width="'. $custom_size .'px" height="'. $custom_size .'px"';
    }else{
      $sizes = 'width="24px" height="24px"';
    }
    return $sizes;
  }

  /**
   * Set buttons' color and size
   * @return string Inline CSS styles
   */
  private function buttons_styles(){
    $color = $this->settings['hpscl_btn_color'];
    $color_value = $this->settings['hpscl_btn_color_value'];

    if( $color !== 'default'){
      $style = '<style type="text/css">';
      $style .= ' svg .iconfill{fill:'. $color_value .';}';
      $style .='</style>';
      return $style;
    }
    return '';
  }


}
