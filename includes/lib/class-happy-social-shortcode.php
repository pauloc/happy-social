<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class happy_social_Shortcode {


  /**
   * The main plugin object.
   * @var   object
   * @access  public
   * @since   1.0.0
   */
  public $parent = null;

  /**
   * The array of taxonomy arguments
   * @var   array
   * @access  public
   * @since   1.0.0
   */
  public $taxonomy_args;

  public function __construct ( $parent ) {
    // Get parent for access
    $this->parent = $parent;

    // Register shortcode
    add_shortcode( 'happy-social', array( $this, 'return_shortcode' ) );

  }


  /**
   * Return social icons
   *
   * @since  1.0 [<description>]
   * @return string
   */
  public function return_shortcode( $atts ) {
    // Get output from front Class
    $front_class = $this->parent->front;
    $output = $front_class->output;

    return '<div class="hpscl hpscl_shortcode">'. $output .'</div>';
  }

}
