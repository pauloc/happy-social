<?php

if ( ! defined( 'ABSPATH' ) ) exit;

class happy_social_Settings {

	/**
	 * The single instance of happy_social_Settings.
	 * @var 	object
	 * @access  private
	 * @since 	1.0.0
	 */
	private static $_instance = null;

	/**
	 * The main plugin object.
	 * @var 	object
	 * @access  public
	 * @since 	1.0.0
	 */
	public $parent = null;

	/**
	 * Prefix for plugin settings.
	 * @var     string
	 * @access  public
	 * @since   1.0.0
	 */
	public $base = 'hpscl';

	/**
	 * Available settings for plugin.
	 * @var     array
	 * @access  public
	 * @since   1.0.0
	 */
	public $settings = array();

	/**
	 * Available networks for plugin.
	 * @var     array
	 * @access  private
	 * @since   1.0.0
	 */
	private $networks = array();

	public function __construct ( $parent ) {
		$this->parent = $parent;

		$this->base = 'hpscl_';

		// Initialise settings
		add_action( 'init', array( $this, 'init_settings' ), 11 );

		// Register plugin settings
		add_action( 'admin_init' , array( $this, 'register_settings' ) );

		// Add settings page to menu
		add_action( 'admin_menu' , array( $this, 'add_menu_item' ) );

		// Add settings link to plugins page
		add_filter( 'plugin_action_links_' . plugin_basename( $this->parent->file ) , array( $this, 'add_settings_link' ) );

		// Add social networks
		add_action('plugins_loaded', array( $this, 'set_networks' ) );

	}


	/**
	 * Initialise settings
	 * @return void
	 */
	public function init_settings () {
		$this->settings = $this->settings_fields();
	}

	/**
	 * Add settings page to admin menu
	 * @return void
	 */
	public function add_menu_item () {
		$page = add_options_page( __( 'Happy Social', 'happy-social' ) , __( 'Happy Social', 'happy-social' ) , 'manage_options' , $this->parent->_token . '_settings' ,  array( $this, 'settings_page' ) );
		add_action( 'admin_print_styles-' . $page, array( $this, 'settings_assets' ) );
	}

	/**
	 * Load settings JS & CSS
	 * @return void
	 */
	public function settings_assets () {

		// We're including the farbtastic script & styles here because they're needed for the colour picker
		// If you're not including a colour picker field then you can leave these calls out as well as the farbtastic dependency for the wpt-admin-js script below
		wp_enqueue_style( 'farbtastic' );
  	wp_enqueue_script( 'farbtastic' );

  	// We're including the WP media scripts here because they're needed for the image upload field
  	// If you're not including an image upload then you can leave this function call out
  	// wp_enqueue_media();

  	wp_register_script( $this->parent->_token . '-settings-js', $this->parent->assets_url . 'js/settings' . $this->parent->script_suffix . '.js', array( 'farbtastic', 'jquery-ui-sortable', 'jquery-ui-draggable', 'jquery' ), '1.0.0' );
  	wp_enqueue_script( $this->parent->_token . '-settings-js' );
	}

	/**
	 * Add settings link to plugin list table
	 * @param  array $links Existing links
	 * @return array 		Modified links
	 */
	public function add_settings_link ( $links ) {
		$settings_link = '<a href="options-general.php?page=' . $this->parent->_token . '_settings">' . __( 'Settings', 'happy-social' ) . '</a>';
  		array_push( $links, $settings_link );
  		return $links;
	}

	/**
	 * Build settings fields
	 * @return array Fields to be displayed on settings page
	 */
	private function settings_fields () {

		$post_types = $this->get_registered_post_types();

		$settings['standard'] = array(
			'title'					=> __( 'Happy Social Settings', 'happy-social' ),
			'description'			=> __( 'These are your customizable options.', 'happy-social' ),

			'fields'				=> array(

				array(
					'id' 			=> 'load_css',
					'label'			=> __( 'Don´t load CSS in front', 'happy-social' ),
					'description'	=> __( 'Check this if you don´t want to load plugin CSS.', 'happy-social' ),
					'type'			=> 'checkbox',
					'placeholder'	=> __( '', 'happy-social' ),
					'default'		=> ''
				),

				array(
					'id' 			=> 'delete_settings',
					'label'			=> __( 'Delete settings on uninstall', 'happy-social' ),
					'description'	=> __( 'Check this if you want to delete plugins settings on uninstall.', 'happy-social' ),
					'type'			=> 'checkbox',
					'placeholder'	=> __( '', 'happy-social' ),
					'default'		=> ''
				),

				array(
					'id' 			=> 'post_types',
					'label'			=> __( 'Post types', 'happy-social' ),
					'description'	=> __( 'You can select multiple post types to show the buttons on.', 'happy-social' ),
					'type'			=> 'checkbox_multi',
					'options'		=> $post_types,
					'default'		=> $post_types
				),

				array(
					'id' 			=> 'btn_size',
					'label'			=> __( 'Buttons´ size', 'happy-social' ),
					'description'	=> __( 'Select buttons´ size.', 'happy-social' ),
					'type'			=> 'radio',
					'options'		=> array( 'small' => 'Small', 'medium' => 'Medium', 'large' => 'Large' ),
					'default'		=> ''
				),

				array(
					'id' 			=> 'small_icon_size',
					'label'			=> __( 'Small icons size', 'happy-social' ),
					'description'	=> __( 'Input the size of small icons in pixels.', 'happy-social' ),
					'type'			=> 'number',
					'placeholder'	=> __( '24', 'happy-social' ),
					'default'		=> '24'
				),
				array(
					'id' 			=> 'medium_icon_size',
					'label'			=> __( 'Medium icons size', 'happy-social' ),
					'description'	=> __( 'Input the size of medium icons in pixels.', 'happy-social' ),
					'type'			=> 'number',
					'placeholder'	=> __( '48', 'happy-social' ),
					'default'		=> '48'
				),
				array(
					'id' 			=> 'large_icon_size',
					'label'			=> __( 'Large icons size', 'happy-social' ),
					'description'	=> __( 'Input the size of large icons in pixels.', 'happy-social' ),
					'type'			=> 'number',
					'placeholder'	=> __( '64', 'happy-social' ),
					'default'		=> '64'
				),

				array(
					'id' 			=> 'btn_color',
					'label'			=> __( 'Buttons´ color', 'happy-social' ),
					'description'	=> __( 'Select buttons´ color.', 'happy-social' ),
					'type'			=> 'radio',
					'options'		=> array( 'default' => 'Default', 'custom' => 'Custom' ),
					'default'		=> 'default'
				),

				array(
					'id' 			=> 'btn_color_value',
					'label'			=> __( 'Pick a colour', 'happy-social' ),
					'description'	=> __( 'Choose a color for your buttons.', 'happy-social' ),
					'type'			=> 'color',
					'default'		=> '#21759B'
				),

				array(
					'id' 			=> 'locations',
					'label'			=> __( 'Locations', 'happy-social' ),
					'description'	=> __( 'You can select multiple locations to show the buttons.', 'happy-social' ),
					'type'			=> 'checkbox_multi',
					'options'		=> array( 'bt' => 'Below Title', 'lf' => 'Left Floating', 'ac' => 'After Content', 'it' => 'Inside Thumbnail' ),
					'default'		=> ''
				),

				array(
					'id' 			=> 'networks',
					'label'			=> __( 'Networks', 'happy-social' ),
					'description'	=> __( 'You can select multiple locations to show the buttons.', 'happy-social' ),
					'type'			=> 'checkbox_multi',
					'options'		=> $this->networks,
					'default'		=> ''
				),

				array(
					'id' 			=> 'networks_order',
					'label'			=> __( 'Networks order', 'happy-social' ),
					'description'	=> __( 'Drag and Drop to select the order you want the buttons to be shown.', 'happy-social' ),
					'type'			=> 'hidden',
					'placeholder'	=> __( 'Placeholder text', 'happy-social' ),
					'default'		=> ''
				),

			)
		);

		$settings = apply_filters( $this->parent->_token . '_settings_fields', $settings );

		return $settings;
	}

	/**
	 * Register plugin settings
	 * @return void
	 */
	public function register_settings () {
		if ( is_array( $this->settings ) ) {

			// Check posted/selected tab
			$current_section = '';
			if ( isset( $_POST['tab'] ) && $_POST['tab'] ) {
				$current_section = $_POST['tab'];
			} else {
				if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
					$current_section = $_GET['tab'];
				}
			}

			foreach ( $this->settings as $section => $data ) {

				if ( $current_section && $current_section != $section ) continue;

				// Add section to page
				add_settings_section( $section, $data['title'], array( $this, 'settings_section' ), $this->parent->_token . '_settings' );

				foreach ( $data['fields'] as $field ) {

					// Validation callback for field
					$validation = '';
					if ( isset( $field['callback'] ) ) {
						$validation = $field['callback'];
					}

					// Register field
					$option_name = $this->base . $field['id'];
					register_setting( $this->parent->_token . '_settings', $option_name, $validation );

					// Add field to page
					add_settings_field( $field['id'], $field['label'], array( $this->parent->admin, 'display_field' ), $this->parent->_token . '_settings', $section, array( 'field' => $field, 'prefix' => $this->base ) );
				}

				if ( ! $current_section ) break;
			}
		}
	}

	public function settings_section ( $section ) {
		$html = '<p> ' . $this->settings[ $section['id'] ]['description'] . '</p>' . "\n";
		echo $html;
	}

	/**
	 * Load settings page content
	 * @return void
	 */
	public function settings_page () {

		// Build page HTML
		$html = '<div class="wrap" id="' . $this->parent->_token . '_settings">' . "\n";
			$html .= '<h2>' . __( 'Plugin Settings' , 'happy-social' ) . '</h2>' . "\n";

			$tab = '';
			if ( isset( $_GET['tab'] ) && $_GET['tab'] ) {
				$tab .= $_GET['tab'];
			}

			// Show page tabs
			if ( is_array( $this->settings ) && 1 < count( $this->settings ) ) {

				$html .= '<h2 class="nav-tab-wrapper">' . "\n";

				$c = 0;
				foreach ( $this->settings as $section => $data ) {

					// Set tab class
					$class = 'nav-tab';
					if ( ! isset( $_GET['tab'] ) ) {
						if ( 0 == $c ) {
							$class .= ' nav-tab-active';
						}
					} else {
						if ( isset( $_GET['tab'] ) && $section == $_GET['tab'] ) {
							$class .= ' nav-tab-active';
						}
					}

					// Set tab link
					$tab_link = add_query_arg( array( 'tab' => $section ) );
					if ( isset( $_GET['settings-updated'] ) ) {
						$tab_link = remove_query_arg( 'settings-updated', $tab_link );
					}

					// Output tab
					$html .= '<a href="' . $tab_link . '" class="' . esc_attr( $class ) . '">' . esc_html( $data['title'] ) . '</a>' . "\n";

					++$c;
				}

				$html .= '</h2>' . "\n";
			}

			$html .= '<form method="post" action="options.php" enctype="multipart/form-data">' . "\n";

				// Get settings fields
				ob_start();
				settings_fields( $this->parent->_token . '_settings' );
				do_settings_sections( $this->parent->_token . '_settings' );
				$html .= ob_get_clean();

				$html .= $this->sortable_setup();

				$html .= '<p class="submit">' . "\n";
					$html .= '<input type="hidden" name="tab" value="' . esc_attr( $tab ) . '" />' . "\n";
					$html .= '<input name="Submit" type="submit" class="button-primary" value="' . esc_attr( __( 'Save Settings' , 'happy-social' ) ) . '" />' . "\n";
				$html .= '</p>' . "\n";
			$html .= '</form>' . "\n";
		$html .= '</div>' . "\n";

		echo $html;
	}


	/**
	 * Get post types
	 *
	 * Retrive registered post types
	 *
	 * @since 1.0.0
	 * @private
	 *
	 */
	private function get_registered_post_types(){
		$args = array(
	  	'public'   => true,
	  	'_builtin' => false
		);
		$output = 'objects'; // names or objects, note names is the default
		$operator = 'and'; // 'and' or 'or'
		$post_types = get_post_types( $args, $output, $operator );
		$types = array();
		foreach ($post_types as $key => $item) {
			$types[$item->name] = $item->labels->singular_name;
		}
		// Add built in types
		$types['page'] = 'Pages';
		$types['post'] = 'Posts';

		return $types;
	}


	/**
	 * Generate network list
	 *
	 * @since 1.0
	 * @public
	 * @filtrable
	 */
	public function set_networks(){
		$this->networks = array(
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'googleplus' => 'GooglePlus',
			'pinterest' => 'Pinterest',
			'linkedin' => 'LinkedIn',
			'whatsapp' => 'Whatsapp',
		);

		/**
		 * Filters the networks list.
		 *
		 * @since 1.0
		 *
		 * @param array $networks The networks array.
		 */
		return apply_filters( 'hpscl_networkslist', $this->networks );
	}

	/**
	 * Generate sortable elements
	 * @return string The elements to sort.
	 */
	private function sortable_setup(){
		$html = '<ul id="networks_sortable">';

		$html .= '</ul>';
		return $html;
	}

	/**
	 * Main happy_social_Settings Instance
	 *
	 * Ensures only one instance of happy_social_Settings is loaded or can be loaded.
	 *
	 * @since 1.0.0
	 * @static
	 * @see happy_social()
	 * @return Main happy_social_Settings instance
	 */
	public static function instance ( $parent ) {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self( $parent );
		}
		return self::$_instance;
	} // End instance()

	/**
	 * Cloning is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __clone () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), $this->parent->_version );
	} // End __clone()

	/**
	 * Unserializing instances of this class is forbidden.
	 *
	 * @since 1.0.0
	 */
	public function __wakeup () {
		_doing_it_wrong( __FUNCTION__, __( 'Cheatin&#8217; huh?' ), $this->parent->_version );
	} // End __wakeup()

}