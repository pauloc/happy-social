jQuery( document ).ready( function ( $ ) {

  var config = {
    // protocol: ['http', 'https'].indexOf(window.location.href.split(':')[0]) === -1 ? 'https://' : '//',
    protocol: 'https://',
    baseurl: WPURL.siteurl,
    url: window.location.href,
    title: document.querySelector('title').innerText,
    // description: document.getElementsByTagName('meta').namedItem('description') ? document.getElementsByTagName('meta').namedItem('description').getAttribute('content') : '',
    image: $('img.wp-post-image').attr("src")
  }
  /**
   * Generate sharing links
   * @param  string data json with plugin's saved settings
   * @return void
   */
  function create_links( data ){
    var saved_networks = data['hpscl_networks_order'];
    if( saved_networks && saved_networks !== undefined){
      var networks = saved_networks.split(',');
      $.each(networks, function(i, v){
        var $icon = $('.' + v + '_icon');
        $icon.on('click', fill_modal);
      });
    }
  }


  /**
   * Open sharing modal
   * @param  object e CLick Event
   * @return void
   */
  function fill_modal ( e ){
    var current_network = e.currentTarget.classList[0].split("_")[0];

    // Exclude whatsapp in mobile
    if( ! isMobileDevice() && current_network === 'whatsapp' ) return;

    switch ( current_network ) {
      case 'twitter':
        var url = config.protocol + 'twitter.com/home?status=';
        url += encodeURIComponent(config.title) + encodeURIComponent(config.url);
        break;
      case 'pinterest':
        var url = config.protocol + 'pinterest.com/pin/create/bookmarklet/?is_video=false';
        url += '&media=' + encodeURIComponent(config.image);
        url += '&url=' + encodeURIComponent(config.url);
        // url += '&description=' + encodeURIComponent(config.title);
        break;
      case 'facebook':
        var url = config.protocol + 'www.facebook.com/share.php?';
        url += 'u=' + encodeURIComponent(config.url);
        url += '&title=' + encodeURIComponent(config.title);

        break;
      case 'googleplus':
        var url = config.protocol + 'plus.google.com/share?';
        url += 'url=' + encodeURIComponent(config.url);
        break;
      case 'linkedin':
        var url = config.protocol + 'www.linkedin.com/shareArticle?mini=true';
        url += '&url=' + encodeURIComponent(config.url);
        url += '&title=' + encodeURIComponent(config.title);
        url += '&source=' + encodeURIComponent(config.source);
        break;
      case 'whatsapp':
        var url = 'whatsapp://send?';
        url += 'text=' + encodeURIComponent(config.title) + ' - ' + encodeURIComponent(config.url);
        break;
    }

    // Launch popup
    window.open( url, 'targetWindow','toolbar=no,location=0,status=no,menubar=no,scrollbars=yes,resizable=yes,width=600,height=250');
  }


  /**
   * Check for mobile devices
   * @return {Boolean} true if mobile
   */
  function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
  };


  /**
   * Get remote WP REST API settings data
   * @return string JSON object with settings
   */
  function getData() {
    return $.ajax( {
      url: config.baseurl + '/wp-json/hpscl/v1/hpscl_options',
      type: 'GET'
    });
  }


  // Start app
  getData().done( create_links );

});
