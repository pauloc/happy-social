jQuery(document).ready(function($) {

  /***** Colour picker *****/

  $('.colorpicker').hide();
  $('.colorpicker').each( function() {
    $(this).farbtastic( $(this).closest('.color-picker').find('.color') );
  });

  $('.color').click(function() {
    $(this).closest('.color-picker').find('.colorpicker').fadeIn();
  });

  $(document).mousedown(function() {
    $('.colorpicker').each(function() {
      var display = $(this).css('display');
      if ( display == 'block' )
        $(this).fadeOut();
    });
  });


  /***** Custom color reveal picker *****/
  if($('#btn_color_default').is(':checked')) {
    $('.color-picker').parent().parent().hide();
  }

  $('#btn_color_default').click(function() {
    if($('#btn_color_default').is(':checked')) {
      $('.color-picker').parent().parent().fadeOut();
    }
  });
  $('#btn_color_custom').click(function() {
    if($('#btn_color_custom').is(':checked')) {
      $('.color-picker').parent().parent().fadeIn();
    }
  });



  /*** Dragabble networks ***/
  var $hpscl_sortable = $("#networks_sortable");
  $hpscl_sortable.sortable({
    revert: true,
    cursor: "move",
    helper: "clone",
    start: function( e, ui ) {},
    stop: function( e, ui ) {
      sortValues();
    }
  });
  $hpscl_sortable.disableSelection();


  /*** Selecting networks ***/

  // Prefetch on load
  var v = $('#networks_order').val();
  if( v !== undefined){
    var arr = v.split(',');
    $.each( arr, function( i, v ){
      if(v){
        $hpscl_sortable.append('<li class="' + v + '"></li>');
      }
    });
  }

  // Select checkboxes
  $nets = $('input[name="hpscl_networks[]"]');
  $nets.change(function() {
    var value = this.value;
    if(this.checked) {
      addItem( value );
    }else{
      removeItem( value);
    }
  });

  // Add item to network's sort list
  var addItem = function(value){
    // Add item
    $hpscl_sortable.append('<li class="' + value + '"></li>');

    // Add value
    $('#networks_order').val(function(i, v) {
      if( v !== undefined){
        var arr = v.split(',');
        arr.push(value);
        return arr.join(',');
      }
    });
  }

  // Remove item to network's sort list
  var removeItem = function(value){
    // Remove item
    $('#networks_sortable .' + value).remove();

    // Remove value
    $('#networks_order').val(function(i, v) {
      return $.grep(v.split(','), function(v) {
        return v != value;
      }).join(',');
    });
  }

  var sortValues = function(){
    var items = $hpscl_sortable.children();
    var ordered = [];

    $.each(items, function(i, v){
      ordered.push(v.className);
    })

    $('#networks_order').val(ordered);
  }

});