=== happy_social ===
Contributors: pauloc
Donate link: todo
Tags: wordpress, plugin, social, social-sharing, sharing, share, Facebook, Twitter, Google+, Pinterest, LinkedIn, Whatsapp
Requires at least: 4.7
Tested up to: 4.7
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

A plugin that will automatically display selected social network(s) sharing buttons in posts, pages and/or custom post types.

== Description ==

A plugin that will automatically display selected social network(s) sharing buttons in posts, pages and/or custom post types.
Support for the following social networks is required: Facebook, Twitter, Google+, Pinterest, LinkedIn, Whatsapp (for mobile browsers only).

Release based on https://github.com/hlashbrooke/WordPress-Plugin-Template
Icons from https://dribbble.com/shots/2720859-70-Flat-Social-Icons-for-Sketch-Updated/
by https://dribbble.com/alexisdoreau

== Installation ==

Installing "happy_social" can be done either by searching for "happy_social" via the "Plugins > Add New" screen in your WordPress dashboard, or by using the following steps:

1. Download the plugin via WordPress.org
1. Upload the ZIP file through the 'Plugins > Add New > Upload' screen in your WordPress dashboard
1. Activate the plugin through the 'Plugins' menu in WordPress

== Screenshots ==

1. Description of first screenshot named screenshot-1
2. Description of second screenshot named screenshot-2
3. Description of third screenshot named screenshot-3

== Frequently Asked Questions ==

= What is the plugin template for? =


== Changelog ==

= 1.0 =
* 2017-01-26
* Initial release
