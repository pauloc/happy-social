<?php
/*
 * Plugin Name: Happy Social
 * Version: 1.0
 * Plugin URI: https://www.paulocarvajal.com/
 * Description: A plugin to display selected social network(s) sharing buttons in posts, pages and/or custom post types.
 * Author: Paulo Carvajal
 * Author URI: https://www.paulocarvajal.com/
 * Requires at least: 4.7
 * Tested up to: 4.7
 *
 * Text Domain: happy-social
 * Domain Path: /lang/
 *
 * @package WordPress
 * @author Paulo Carvajal
 * @since 1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) exit;

// Load plugin class files
require_once( 'includes/class-happy-social.php' );
require_once( 'includes/class-happy-social-settings.php' );

// Load plugin libraries
require_once( 'includes/lib/class-happy-social-admin-api.php' );
require_once( 'includes/lib/class-happy-social-rest-api.php' );
require_once( 'includes/lib/class-happy-social-shortcode.php' );
require_once( 'includes/lib/class-happy-social-front.php' );
// require_once( 'includes/lib/class-happy-social-post-type.php' );
// require_once( 'includes/lib/class-happy-social-taxonomy.php' );

/**
 * Returns the main instance of happy_social to prevent the need to use globals.
 *
 * @since  1.0.0
 * @return object happy_social
 */
function happy_social () {
  global $wp_version;
  if ( version_compare( $wp_version, '4.7-alpha', '<' ) ) {
    return $this->markTestSkipped( 'WordPress version not supported.' );
  }


	$instance = happy_social::instance( __FILE__, '1.0.0' );

	if ( is_null( $instance->settings ) ) {
		$instance->settings = happy_social_Settings::instance( $instance );
	}

	return $instance;
}

happy_social();