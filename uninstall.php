<?php

/**
 *
 * This file runs when the plugin in uninstalled (deleted).
 * This will not run when the plugin is deactivated.
 * Ideally you will add all your clean-up scripts here
 * that will clean-up unused meta, options, etc. in the database.
 *
 */

// If plugin is not being uninstalled, exit (do nothing)
if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}

// Do something here if plugin is being uninstalled.

$delete_settings = get_option('hpscl_delete_settings');
if( $delete_settings == 'on' ){
  $settings = array(
    'happy_social_version',
    'hpscl_post_types',
    'hpscl_btn_size',
    'hpscl_btn_color',
    'hpscl_btn_color_value',
    'hpscl_locations',
    'hpscl_networks',
    'hpscl_networks_order',
    'hpscl_load_css',
    'hpscl_large_icon_size',
    'hpscl_medium_icon_size',
    'hpscl_small_icon_size',
    'hpscl_delete_settings',
  );
  foreach ($settings as $value) {
    delete_option( $value );
  }
}


